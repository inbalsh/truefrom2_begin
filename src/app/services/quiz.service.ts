import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  constructor(private http: HttpClient) { }

  get(url: string) {
    return this.http.get(url);
  }

  getAll() {
    return [
      { id: 'data/quiz1.json', name: 'quiz 1: Easy' },
      { id: 'data/quiz2.json', name: 'quiz 2: Medium' },
      { id: 'data/quiz3.json', name: 'quiz 3: Hard' }
    ];
  }



}
